package nl.makertim.voidblock.world;

import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.BiomeProvider;
import net.minecraft.world.biome.BiomeProviderSingle;
import net.minecraft.world.gen.IChunkGenerator;

public class VoidblockWorldType extends WorldType {


	public VoidblockWorldType(String name) {
		super(name);
	}

	@Override
	public BiomeProvider getBiomeProvider(World world) {
		world.getGameRules().setOrCreateGameRule("announceAdvancements", "false");
		return new BiomeProviderSingle(VoidblockBiome.instance);
	}

	@Override
	public IChunkGenerator getChunkGenerator(World world, String generatorOptions) {
		return new VoidblockChunkGenerator(world);
	}
}
