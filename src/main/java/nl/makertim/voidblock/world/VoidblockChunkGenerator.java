package nl.makertim.voidblock.world;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.ChunkPrimer;
import net.minecraft.world.gen.IChunkGenerator;

import javax.annotation.Nullable;
import java.util.List;

public class VoidblockChunkGenerator implements IChunkGenerator {

	private final World world;
	private ChunkPrimer chunkprimer;

	public VoidblockChunkGenerator(World world) {
		this.world = world;

		IBlockState stoneDefault = Blocks.STONE.getDefaultState();
		IBlockState bedrockDefault = Blocks.BEDROCK.getDefaultState();
		chunkprimer = new ChunkPrimer();
		for (int x = 0; x < 16; x++) {
			for (int z = 0; z < 16; z++) {
				for (int y = 5; y < world.getHeight() - 5; y++) {
					chunkprimer.setBlockState(x, y, z, stoneDefault);
				}
				for (int y = 0; y < 5; y++) {
					chunkprimer.setBlockState(x, y, z, bedrockDefault);
					chunkprimer.setBlockState(x, world.getHeight() - y - 1, z, bedrockDefault);
				}
			}
		}
	}

	@Override
	public Chunk generateChunk(int xChunk, int zChunk) {
		Chunk chunk = new Chunk(world, chunkprimer, xChunk, zChunk);
		byte[] abyte = chunk.getBiomeArray();
		for (int i = 0; i < abyte.length; i++) {
			abyte[i] = (byte) Biome.getIdForBiome(VoidblockBiome.instance);
		}
		chunk.generateSkylightMap();

		return chunk;
	}

	@Override
	public void populate(int x, int z) {
	}

	@Override
	public boolean generateStructures(Chunk chunkIn, int x, int z) {
		return false;
	}

	@Override
	public List<Biome.SpawnListEntry> getPossibleCreatures(EnumCreatureType creatureType, BlockPos pos) {
		return VoidblockBiome.instance.getSpawnableList(creatureType);
	}

	@Nullable
	@Override
	public BlockPos getNearestStructurePos(World worldIn, String structureName, BlockPos position, boolean findUnexplored) {
		return null;
	}

	@Override
	public void recreateStructures(Chunk chunkIn, int x, int z) {
	}

	@Override
	public boolean isInsideStructure(World worldIn, String structureName, BlockPos pos) {
		return false;
	}
}
