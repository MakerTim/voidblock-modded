package nl.makertim.voidblock.world;

import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeDecorator;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.terraingen.DecorateBiomeEvent;
import net.minecraftforge.event.terraingen.TerrainGen;

import java.util.Random;

public class VoidblockDecorator extends BiomeDecorator {


	public VoidblockDecorator() {
		sandPatchesPerChunk = 15;
		clayPerChunk = 32;
		grassPerChunk = 32;
	}

	protected void genDecorations(Biome biomeIn, World worldIn, Random random) {
		ChunkPos forgeChunkPos = new ChunkPos(chunkPos);
		MinecraftForge.EVENT_BUS.post(new DecorateBiomeEvent.Pre(worldIn, random, forgeChunkPos));
		this.generateOres(worldIn, random);

		if (TerrainGen.decorate(worldIn, random, forgeChunkPos, DecorateBiomeEvent.Decorate.EventType.SAND)) {
			generate(sandPatchesPerChunk, sandGen, worldIn, random);
		}

		if (TerrainGen.decorate(worldIn, random, forgeChunkPos, DecorateBiomeEvent.Decorate.EventType.CLAY)) {
			generate(clayPerChunk, clayGen, worldIn, random);
		}

		if (TerrainGen.decorate(worldIn, random, forgeChunkPos, DecorateBiomeEvent.Decorate.EventType.SAND_PASS2)) {
			generate(gravelPatchesPerChunk, gravelGen, worldIn, random);
		}

		net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(new DecorateBiomeEvent.Post(worldIn, random, forgeChunkPos));
	}

	private void generate(int perChunk, WorldGenerator generator, World worldIn, Random random) {
		for (int patch = 0; patch < perChunk; ++patch) {
			int x = random.nextInt(16) + 8;
			int y = random.nextInt(worldIn.getHeight());
			int z = random.nextInt(16) + 8;
			generator.generate(worldIn, random, this.chunkPos.add(x, y, z));
		}
	}
}
