package nl.makertim.voidblock.world;

import net.minecraft.world.WorldType;

public class Worldgen {

	private static WorldType voidblockWorldType;


	public static void init() {
		voidblockWorldType = new VoidblockWorldType("voidblock");

		switchWorldTypeDefault();
	}

	private static void switchWorldTypeDefault() {
		int id = voidblockWorldType.getId();
		WorldType defaultType = WorldType.WORLD_TYPES[0];
		WorldType.WORLD_TYPES[0] = voidblockWorldType;
		WorldType.WORLD_TYPES[id] = defaultType;
	}
}
