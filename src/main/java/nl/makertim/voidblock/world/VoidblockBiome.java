package nl.makertim.voidblock.world;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeDecorator;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import nl.makertim.voidblock.VoidblockMod;

public class VoidblockBiome extends Biome {

	public static final VoidblockBiome instance = new VoidblockBiome();

	public VoidblockBiome() {
		super(new BiomeProperties("voidblockbiome")
				.setRainfall(0F)
				.setHeightVariation(0F)
				.setTemperature(0F)
				.setRainDisabled()
		);
		setRegistryName(VoidblockMod.MODID + ":voidblockbiome");
	}

	@Override
	public BiomeDecorator createBiomeDecorator() {
		return new VoidblockDecorator();
	}

	@Override
	public boolean ignorePlayerSpawnSuitability() {
		return true;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public int getSkyColorByTemp(float currentTemperature) {
		return 0;
	}

}
