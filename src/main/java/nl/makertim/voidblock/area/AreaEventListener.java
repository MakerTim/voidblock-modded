package nl.makertim.voidblock.area;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import nl.makertim.voidblock.event.PlayerActionAreaEvent;
import nl.makertim.voidblock.event.PlayerAreaEvent;
import nl.makertim.voidblock.event.PlayerJoinedAreaEvent;

public class AreaEventListener {

	private AreaSystem areaSystem;

	public AreaEventListener(AreaSystem areaSystem) {
		this.areaSystem = areaSystem;
	}

	@SubscribeEvent
	public void onTick(TickEvent.PlayerTickEvent playerTickEvent) {
		if (playerTickEvent.phase != TickEvent.Phase.START) {
			return;
		}
		// once a second
		if (playerTickEvent.player.world.getTotalWorldTime() % 20 == 0) {
			// check if player is in area
		}
	}

	public boolean checkIsAllowedInArea(EntityPlayer player, Area area) {
		return checkInArea(new PlayerJoinedAreaEvent(player, area));
	}

	public boolean checkCanDoInArea(EntityPlayer player, Area area) {
		return checkInArea(new PlayerActionAreaEvent(player, area));
	}

	public boolean checkInArea(PlayerAreaEvent event) {
		MinecraftForge.EVENT_BUS.post(event);
		return event.isCanceled();
	}

}
