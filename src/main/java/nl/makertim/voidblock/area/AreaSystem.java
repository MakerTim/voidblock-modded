package nl.makertim.voidblock.area;

import net.minecraftforge.common.MinecraftForge;

public class AreaSystem {

	private AreaEventListener listener = new AreaEventListener(this);

	public void init(){
		MinecraftForge.EVENT_BUS.register(listener);
	}

}
