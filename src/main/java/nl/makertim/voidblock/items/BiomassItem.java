package nl.makertim.voidblock.items;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFlowerPot;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import nl.makertim.voidblock.recipes.Recipes;

import java.util.Optional;

public class BiomassItem extends Item {

	public BiomassItem() {
		this.maxStackSize = 10;
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		ItemStack itemstack = playerIn.getHeldItem(handIn);
		RayTraceResult raytraceresult = this.rayTrace(worldIn, playerIn, false);

		if (raytraceresult == null || raytraceresult.typeOfHit != RayTraceResult.Type.BLOCK) {
			return new ActionResult<>(EnumActionResult.PASS, itemstack);
		} else {
			BlockPos blockpos = raytraceresult.getBlockPos();
			TileEntity te = worldIn.getTileEntity(blockpos);
			if (worldIn.isBlockModifiable(playerIn, blockpos) &&
					playerIn.canPlayerEdit(blockpos, raytraceresult.sideHit, itemstack)) {
				if (te instanceof TileEntityFlowerPot) {
					TileEntityFlowerPot tileEntityFlowerPot = (TileEntityFlowerPot) te;
					if (tileEntityFlowerPot.getFlowerPotItem() == new ItemStack(Blocks.DEADBUSH).getItem()) {
						tileEntityFlowerPot.setItemStack(new ItemStack(Blocks.SAPLING, 1, 1));
					} else {
						tileEntityFlowerPot.setItemStack(new ItemStack(Blocks.DEADBUSH));
					}
					worldIn.notifyBlockUpdate(blockpos, worldIn.getBlockState(blockpos), worldIn.getBlockState(blockpos), 2);
					tileEntityFlowerPot.markDirty();
					itemstack.shrink(1);
					return new ActionResult<>(EnumActionResult.SUCCESS, itemstack);
				} else if (worldIn.getBlockState(blockpos).getBlock() == Blocks.GRASS &&
						worldIn.getBlockState(blockpos.offset(EnumFacing.UP)).getBlock() == Blocks.AIR &&
						!worldIn.isRemote) {
					IBlockState blockState = randomGarden();
					worldIn.setBlockState(blockpos.offset(EnumFacing.UP), blockState);

				}
			}
		}
		return new ActionResult<>(EnumActionResult.FAIL, itemstack);
	}

	@SuppressWarnings("deprecation")
	private IBlockState randomGarden() {
		if (Math.random() < 0.01D) {
			return Blocks.SAPLING.getStateFromMeta(1);
		}
		return Optional.ofNullable(Block.getBlockFromName(Recipes.biomassRecipes.get((int) (Math.random() * Recipes.biomassRecipes.size()))))
				.orElse(Blocks.DEADBUSH).getDefaultState();
	}
}
