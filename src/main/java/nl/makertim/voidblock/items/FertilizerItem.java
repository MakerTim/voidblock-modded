package nl.makertim.voidblock.items;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class FertilizerItem extends Item {

	public FertilizerItem() {
		this.maxStackSize = 10;
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		ItemStack itemstack = playerIn.getHeldItem(handIn);
		RayTraceResult raytraceresult = this.rayTrace(worldIn, playerIn, false);

		if (raytraceresult == null || raytraceresult.typeOfHit != RayTraceResult.Type.BLOCK) {
			return new ActionResult<>(EnumActionResult.PASS, itemstack);
		} else {
			BlockPos blockpos = raytraceresult.getBlockPos();
			if (worldIn.isBlockModifiable(playerIn, blockpos) &&
					playerIn.canPlayerEdit(blockpos, raytraceresult.sideHit, itemstack) &&
					worldIn.getBlockState(blockpos).getBlock() == Blocks.DIRT) {
				worldIn.setBlockState(blockpos, Blocks.GRASS.getDefaultState());
				worldIn.notifyBlockUpdate(blockpos, Blocks.DIRT.getDefaultState(), Blocks.GRASS.getDefaultState(), 2);
				worldIn.playSound(playerIn, blockpos, SoundEvents.ITEM_HOE_TILL, SoundCategory.BLOCKS, 1.0F, 1.0F);
				itemstack.shrink(1);
				return new ActionResult<>(EnumActionResult.SUCCESS, itemstack);
			}
		}
		return new ActionResult<>(EnumActionResult.FAIL, itemstack);
	}
}
