package nl.makertim.voidblock.items;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import nl.makertim.voidblock.blocks.StoneAnvileTileEntity;
import nl.makertim.voidblock.recipes.Recipes;

public class HammerItem extends ItemPickaxe {

	static {
		MinecraftForge.EVENT_BUS.register(HammerItem.class);
	}

	protected HammerItem(ToolMaterial material) {
		super(material);
	}

	@Override
	public boolean onBlockStartBreak(ItemStack itemstack, BlockPos pos, EntityPlayer player) {
		TileEntity tileEntity = player.world.getTileEntity(pos);
		if (tileEntity instanceof StoneAnvileTileEntity) {
			StoneAnvileTileEntity anvileTileEntity = (StoneAnvileTileEntity) tileEntity;
			anvileTileEntity.doStep();
			return true;
		}
		return super.onBlockStartBreak(itemstack, pos, player);
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public static void hammer(BlockEvent.HarvestDropsEvent event) {
		EntityPlayer player = event.getHarvester();
		if (player == null) {
			return;
		}

		ItemStack is = player.getHeldItemMainhand();
		if (is.isEmpty() || !(is.getItem() instanceof HammerItem)) {
			return;
		}
		Block blockMined = event.getState().getBlock();
		Recipes.hammerRecipes.computeIfPresent(blockMined, (block, itemStack) -> {
			event.getDrops().clear();
			event.setDropChance(1F);
			event.getDrops().add(itemStack.copy());
			return itemStack;
		});
	}

}
