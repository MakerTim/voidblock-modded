package nl.makertim.voidblock.items;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;

public class StoneBucket extends DustItem {

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		ItemStack itemstack = playerIn.getHeldItem(handIn);
		RayTraceResult raytraceresult = this.rayTrace(worldIn, playerIn, true);

		if (raytraceresult == null) {
			return new ActionResult<>(EnumActionResult.PASS, itemstack);
		} else if (raytraceresult.typeOfHit != RayTraceResult.Type.BLOCK) {
			return new ActionResult<>(EnumActionResult.PASS, itemstack);
		} else {
			BlockPos blockpos = raytraceresult.getBlockPos();
			EnumFacing facing = raytraceresult.sideHit;
			TileEntity te = worldIn.getTileEntity(blockpos);
			if (te == null || !te.hasCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, facing)) {
				return super.onItemRightClick(worldIn, playerIn, handIn);
			}

			IFluidHandler fluidHandler = te.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, facing);
			FluidStack stack = fluidHandler.drain(Fluid.BUCKET_VOLUME, false);
			if (stack == null || stack.amount != Fluid.BUCKET_VOLUME) {
				return ActionResult.newResult(EnumActionResult.FAIL, itemstack);
			} else {
				fluidHandler.drain(Fluid.BUCKET_VOLUME, true);
				dropWater(worldIn, playerIn.getPosition(), te.getPos(), stack.getFluid().getBlock().getDefaultState());
			}
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override
	protected boolean isAcceptedType(EntityPlayer player, BlockPos pos) {
		World world = player.world;
		IBlockState state = world.getBlockState(pos);

		if (!world.isRemote && state.getMaterial().isLiquid()) {
			world.setBlockState(pos, Blocks.AIR.getDefaultState(), 11);
			dropWater(world, player.getPosition(), pos, state);
			return true;
		}

		return false;
	}

	private void dropWater(World world, BlockPos posDrop, BlockPos posFrom, IBlockState state) {
		if (!world.isRemote) {
			world.setBlockState(posDrop, state, 11);
			state.neighborChanged(world, posDrop, state.getBlock(), posDrop);
		}
		world.playSound(null, posFrom, SoundEvents.ITEM_BUCKET_FILL, SoundCategory.BLOCKS, 1.0F, 1.0F);
		world.playSound(null, posDrop, SoundEvents.ITEM_BUCKET_EMPTY, SoundCategory.BLOCKS, 1.0F, 1.0F);
	}


	@Override
	protected ItemStack handleConversion(EntityPlayer player, ItemStack current) {
		return current;
	}
}
