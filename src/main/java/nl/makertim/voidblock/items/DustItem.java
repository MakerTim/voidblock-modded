package nl.makertim.voidblock.items;

import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import nl.makertim.voidblock.VoidblockMod;

public class DustItem extends Item {

	public DustItem() {
		this.maxStackSize = 32;
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		ItemStack itemstack = playerIn.getHeldItem(handIn);
		RayTraceResult raytraceresult = this.rayTrace(worldIn, playerIn, true);

		if (raytraceresult == null) {
			return new ActionResult<>(EnumActionResult.PASS, itemstack);
		} else if (raytraceresult.typeOfHit != RayTraceResult.Type.BLOCK) {
			return new ActionResult<>(EnumActionResult.PASS, itemstack);
		} else {
			BlockPos blockpos = raytraceresult.getBlockPos();
			if (!worldIn.isBlockModifiable(playerIn, blockpos)) {
				return new ActionResult<>(EnumActionResult.FAIL, itemstack);
			} else {
				if (!playerIn.canPlayerEdit(blockpos.offset(raytraceresult.sideHit), raytraceresult.sideHit, itemstack)) {
					return new ActionResult<>(EnumActionResult.FAIL, itemstack);
				} else {
					if (isAcceptedType(playerIn, blockpos)) {
						playerIn.addStat(StatList.getObjectUseStats(this));
						return new ActionResult<>(EnumActionResult.SUCCESS, handleConversion(playerIn, itemstack));
					} else {
						return new ActionResult<>(EnumActionResult.FAIL, itemstack);
					}
				}
			}
		}
	}

	protected boolean isAcceptedType(EntityPlayer player, BlockPos pos) {
		World world = player.world;
		IBlockState iblockstate = world.getBlockState(pos);
		boolean ret = iblockstate.getMaterial() == Material.WATER && iblockstate.getValue(BlockLiquid.LEVEL) == 0;
		if (ret) {
			if (!world.isRemote && world.rand.nextInt(1000) == 23) {
				world.setBlockState(pos, Blocks.AIR.getDefaultState(), 11);
			}
		}
		return ret;
	}

	protected ItemStack handleConversion(EntityPlayer player, ItemStack current) {
		current.shrink(1);
		VoidblockMod.proxyUtil.giveOrDrop(player, new ItemStack(Items.CLAY_BALL));
		return current;
	}
}
