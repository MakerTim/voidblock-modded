package nl.makertim.voidblock.items;

import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.event.RegistryEvent;
import nl.makertim.voidblock.VoidblockMod;
import nl.makertim.voidblock.blocks.BlockRegistration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class ItemRegistration {

	public static Item rock;
	public static Item shard;
	public static HammerItem stoneHammer, ironHammer, diamondHammer;
	public static DustItem stoneDust;
	public static StoneBucket stoneBucket;
	public static Item algae, algaeDry;
	public static FertilizerItem fertilizer;
	public static BiomassItem biomassa;
	public static List<ItemBlock> itemBlocks;

	public static void init(RegistryEvent.Register<Item> event) {
		rock = new Item().setUnlocalizedName(VoidblockMod.MODID + ".rock").setRegistryName(VoidblockMod.MODID + ":rock");
		shard = new Item().setUnlocalizedName(VoidblockMod.MODID + ".shard").setRegistryName(VoidblockMod.MODID + ":shard");
		stoneDust = (DustItem) new DustItem().setUnlocalizedName(VoidblockMod.MODID + ".stonedust").setRegistryName(VoidblockMod.MODID + ":stonedust");
		stoneBucket = (StoneBucket) new StoneBucket().setUnlocalizedName(VoidblockMod.MODID + ".stonebucket").setRegistryName(VoidblockMod.MODID + ":stonebucket");
		stoneHammer = (HammerItem) new HammerItem(Item.ToolMaterial.GOLD).setUnlocalizedName(VoidblockMod.MODID + ".hammerstone").setRegistryName(VoidblockMod.MODID + ":hammerstone");
		ironHammer = (HammerItem) new HammerItem(Item.ToolMaterial.IRON).setUnlocalizedName(VoidblockMod.MODID + ".hammeriron").setRegistryName(VoidblockMod.MODID + ":hammeriron");
		diamondHammer = (HammerItem) new HammerItem(Item.ToolMaterial.DIAMOND).setUnlocalizedName(VoidblockMod.MODID + ".hammerdiamond").setRegistryName(VoidblockMod.MODID + ":hammerdiamond");
		algae = new Item().setUnlocalizedName(VoidblockMod.MODID + ".algae").setRegistryName(VoidblockMod.MODID + ":algae");
		algaeDry = new Item().setUnlocalizedName(VoidblockMod.MODID + ".algaedry").setRegistryName(VoidblockMod.MODID + ":algaedry");
		fertilizer = (FertilizerItem) new FertilizerItem().setUnlocalizedName(VoidblockMod.MODID + ".fertilizer").setRegistryName(VoidblockMod.MODID + ":fertilizer");
		biomassa = (BiomassItem) new BiomassItem().setUnlocalizedName(VoidblockMod.MODID + ".biomassa").setRegistryName(VoidblockMod.MODID + ":biomassa");

		initBlocks(event);

		getItems().forEach(item -> item.setCreativeTab(VoidblockMod.creativeTab));
		Arrays.stream(getOnlyItems()).forEach(item -> event.getRegistry().register(item));

		if (VoidblockMod.proxyUtil.isClient()) {
			ClientItemRegistration.init();
		}
	}

	public static void initBlocks(RegistryEvent.Register<Item> event) {
		itemBlocks = new ArrayList<>();
		Arrays.stream(BlockRegistration.getBlocks()).forEach(block -> {
			Item blockItem = new ItemBlock(block).setRegistryName(block.getRegistryName() + "item");
			itemBlocks.add((ItemBlock) blockItem);

			event.getRegistry().register(blockItem);
		});
	}

	public static Item[] getOnlyItems() {
		return new Item[]{
				rock,
				shard,
				stoneHammer, ironHammer, diamondHammer,
				stoneDust,
				stoneBucket,
				algae,
				algaeDry,
				biomassa,
				fertilizer,
		};
	}

	public static Collection<Item> getItems() {
		Collection<Item> items = new ArrayList<>(itemBlocks);

		items.addAll(Arrays.asList(getOnlyItems()));

		return items;
	}
}
