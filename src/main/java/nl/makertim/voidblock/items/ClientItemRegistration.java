package nl.makertim.voidblock.items;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import nl.makertim.voidblock.VoidblockMod;
import nl.makertim.voidblock.blocks.StoneCrafterTESR;
import nl.makertim.voidblock.blocks.StoneCrafterTileEntity;

public class ClientItemRegistration {

	public static void init() {
		MinecraftForge.EVENT_BUS.register(ClientItemRegistration.class);
	}

	@SubscribeEvent
	public static void onModelRegister(ModelRegistryEvent event) {
		ItemRegistration.getItems().forEach(item -> {
			ModelLoader.setCustomModelResourceLocation(item, 0,
					new ModelResourceLocation(item.getRegistryName(), null));
		});
	}
}
