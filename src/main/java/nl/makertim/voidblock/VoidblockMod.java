package nl.makertim.voidblock;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.ProgressManager;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import nl.makertim.voidblock.area.AreaSystem;
import nl.makertim.voidblock.blocks.BlockRegistration;
import nl.makertim.voidblock.food.DecayFood;
import nl.makertim.voidblock.items.ItemRegistration;
import nl.makertim.voidblock.load.EntityCapping;
import nl.makertim.voidblock.stages.StagesRegistration;
import nl.makertim.voidblock.util.Util;
import nl.makertim.voidblock.world.VoidblockBiome;
import nl.makertim.voidblock.world.Worldgen;
import org.apache.logging.log4j.Logger;

@Mod(modid = VoidblockMod.MODID, name = VoidblockMod.NAME, version = VoidblockMod.VERSION)
public class VoidblockMod {
	public static final String MODID = "voidblockmodded";
	public static final String NAME = "Voidblock Modded";
	public static final String VERSION = "1.0";

	public static Logger logger;
	public static AreaSystem areaSystem;

	public static CreativeTabs creativeTab = new CreativeTabs(MODID) {
		@Override
		@SideOnly(Side.CLIENT)
		public ItemStack getTabIconItem() {
			return new ItemStack(BlockRegistration.stoneCrafter);
		}
	};

	@SidedProxy(serverSide = "nl.makertim.voidblock.util.ServerUtil", clientSide = "nl.makertim.voidblock.util.ClientUtil")
	public static Util proxyUtil;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		areaSystem = new AreaSystem();
		logger = event.getModLog();
		MinecraftForge.EVENT_BUS.register(this);
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		logger.info("Initialzing VoidBlock");

		ProgressManager.ProgressBar bar = ProgressManager.push("Initializing", 5);
		bar.step("Managing Areas");
		areaSystem.init();
		bar.step("Decaying Food");
		DecayFood.init();
		bar.step("Filling worlds with stone");
		Worldgen.init();
		bar.step("Stuff Entities with TNT");
		EntityCapping.init();
		bar.step("Filling worlds with stone");
		StagesRegistration.init();
		ProgressManager.pop(bar);
	}

	@SubscribeEvent
	public void registerBiomes(RegistryEvent.Register<Biome> event) {
		event.getRegistry().register(VoidblockBiome.instance);
	}

	@SubscribeEvent
	public void registerItems(RegistryEvent.Register<Item> event) {
		ItemRegistration.init(event);
	}

	@SubscribeEvent
	public void registerBlock(RegistryEvent.Register<Block> event) {
		BlockRegistration.init(event);
	}

}
