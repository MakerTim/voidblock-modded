package nl.makertim.voidblock.food;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagByte;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagLong;
import net.minecraft.nbt.NBTTagString;
import net.minecraftforge.event.entity.item.ItemTossEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import nl.makertim.voidblock.VoidblockMod;
import nl.makertim.voidblock.util.Util;
import squeek.applecore.api.food.FoodEvent;
import squeek.applecore.api.food.FoodValues;

import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;

public class FoodEventListener {

	public static final String TAG_PLAYER_TIMER = "voidblock_food_player_timer";
	public static final String TAG_PLAYER_FROM = "voidblock_food_player_from";
	public static final String TAG_DROP = "voidblock_food_dropped";
	public static final BiFunction<Long, Boolean, Double> calculation =
			(ticksPassed, hasDropped) ->
					Math.log(ticksPassed) * 3.6D * (hasDropped ? 2 : 1);

	@SubscribeEvent
	public void onTick(TickEvent.PlayerTickEvent playerTickEvent) {
		if (playerTickEvent.phase != TickEvent.Phase.START) {
			return;
		}
		// once a second
		if (playerTickEvent.player.world.getTotalWorldTime() % 20 == 0) {

			playerTickEvent.player.inventory.mainInventory.stream() //
					.filter(itemStack -> itemStack.getItem() instanceof ItemFood) //
					.filter(itemStack -> !checkItemStack(itemStack, playerTickEvent.player)) //
					.forEach(itemStack -> applyPlayerTag(playerTickEvent.player, itemStack));
		}
	}

	@SubscribeEvent
	public void onCraftFood(PlayerEvent.ItemCraftedEvent event) {
		ItemStack is = event.crafting;
		if (is.getItem() instanceof ItemFood) {
			//TODO: add penalty for using food that is already decayed
			if (event.player != null) {
				addTag(is, event.player);
			}
		}
	}

	@SubscribeEvent
	public void onDrop(ItemTossEvent event) {
		ItemStack is = event.getEntityItem().getItem();
		if (is.getItem() instanceof ItemFood) {
			is.setTagInfo(TAG_DROP, new NBTTagByte((byte) 1));
		}
	}

	@SubscribeEvent
	public void onPickup(PlayerEvent.ItemPickupEvent event) {
		ItemStack is = event.getOriginalEntity().getItem();
		if (is.getItem() instanceof ItemFood) {
			is.setTagInfo(TAG_DROP, new NBTTagByte((byte) 1));
		}
	}

	@SubscribeEvent
	public void onCalculateFood(FoodEvent.GetPlayerFoodValues event) {
		FoodValues foodValues = event.foodValues;
		ItemStack is = event.food;
		long time = timePassed(is, event.player);
		boolean hasBeenDropped = hasBeenDropped(is);

		double penalty = calculation.apply(time, hasBeenDropped);
		penalty = Math.min(1, penalty);
		penalty = Math.max(0.1, penalty);
		int hunger = (int) (foodValues.hunger * penalty);
		float saturation = (float) (foodValues.saturationModifier * penalty);

		event.foodValues = new FoodValues(hunger, saturation);
	}

	private boolean hasBeenDropped(ItemStack itemStack) {
		NBTTagCompound nbt = getFromItemStack(itemStack);
		return nbt.hasKey(TAG_DROP);
	}

	private long timePassed(ItemStack itemStack, EntityPlayer player) {
		NBTTagCompound nbt = getFromItemStack(itemStack);
		if (nbt.hasKey(TAG_PLAYER_TIMER)) {
			return player.ticksExisted - nbt.getLong(TAG_PLAYER_TIMER);
		}
		return 20L * 60 * 60;
	}

	private void addTag(ItemStack itemStack, EntityPlayer player) {
		addTag(itemStack, player, 0);
	}

	private void addTag(ItemStack itemStack, EntityPlayer player, long addon) {
		itemStack.setTagInfo(TAG_PLAYER_FROM, new NBTTagString(player.getCachedUniqueIdString()));
		itemStack.setTagInfo(TAG_PLAYER_TIMER, getTimeUnified(player.ticksExisted - addon));
	}

	private NBTTagLong getTimeUnified(long ticks) {
		return new NBTTagLong(VoidblockMod.proxyUtil.unify(ticks, 3));
	}

	private void applyPlayerTag(EntityPlayer player, ItemStack itemStack) {
		NBTTagCompound nbt = getFromItemStack(itemStack);
		long addon = 0;

		if (nbt.hasKey(TAG_PLAYER_TIMER)) {
			String otherUuidString = nbt.getString(TAG_PLAYER_FROM);
			UUID otherUuid = UUID.fromString(otherUuidString);
			Map<String, Number> otherPlayerStats = VoidblockMod.proxyUtil.fromPlayerStats(player.world, otherUuid);

			addon = otherPlayerStats.getOrDefault("stat.playOneMinute", player.world.getTotalWorldTime() / 2L).longValue();
		}

		addTag(itemStack, player, addon);
	}

	private boolean checkItemStack(ItemStack is, EntityPlayer player) {
		UUID uuid = player.getUniqueID();
		String uuidString = uuid.toString();
		NBTTagCompound nbtTagCompound = getFromItemStack(is);

		return nbtTagCompound.hasKey(TAG_PLAYER_TIMER) &&
				nbtTagCompound.hasKey(TAG_PLAYER_FROM) &&
				nbtTagCompound.getString(TAG_PLAYER_FROM).equals(uuidString);
	}

	private NBTTagCompound getFromItemStack(ItemStack itemStack) {
		NBTTagCompound nbt = itemStack.getTagCompound();
		if (nbt == null) {
			nbt = new NBTTagCompound();
		}
		return nbt;
	}
}
