package nl.makertim.voidblock.recipes;

import com.google.common.collect.Lists;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import nl.makertim.voidblock.items.ItemRegistration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Recipes {

	public static final Map<Block, ItemStack> hammerRecipes = new HashMap<>();
	public static final Map<Item, List<ItemRecipe>> itemRecipes = new HashMap<>();
	public static final List<String> biomassRecipes = new ArrayList<>();

	static {
		registerHammerRecipes();
		registerItemRecipes();
		registerGardens();
	}

	private static void registerHammerRecipes() {
		hammerRecipes.put(Blocks.STONE, new ItemStack(ItemRegistration.stoneDust));
		hammerRecipes.put(Blocks.COBBLESTONE, new ItemStack(Blocks.GRAVEL));
	}

	private static void registerItemRecipes() {
		new ItemRecipe(
				new ItemStack(ItemRegistration.stoneDust),
				new ItemStack(ItemRegistration.algae),
				250,
				0.5F
		).setNeedsWater();
		new ItemRecipe(
				new ItemStack(ItemRegistration.algae),
				new ItemStack(ItemRegistration.algaeDry),
				500,
				2F / 3F
		).setMinimum(0);
		new ItemRecipe(
				new ItemStack(ItemRegistration.algae),
				new ItemStack(ItemRegistration.algae),
				1500,
				1.1F
		).setNeedsWater();
	}

	private static void registerGardens() {
		biomassRecipes.addAll(Lists.newArrayList(
				"harvestcraft:frostgarden",
				"harvestcraft:shadedgarden",
				"harvestcraft:soggygarden",
				"harvestcraft:tropicalgarden",
				"harvestcraft:windygarden"
		));
	}

	public static class ItemRecipe {
		public final ItemStack input;
		public final ItemStack output;
		public final long ticks;
		public final float chance;
		private int minimum = 1;
		private boolean needsWater;
		private boolean inCauldron;

		public ItemRecipe(ItemStack input, ItemStack output, long ticks, float chance) {
			Item inputItem = input.getItem();
			this.input = input;
			this.output = output;
			this.chance = chance;
			this.ticks = ticks;

			List<ItemRecipe> recipes = itemRecipes.getOrDefault(inputItem, new ArrayList<>());
			recipes.add(this);
			itemRecipes.put(inputItem, recipes);
		}

		public ItemRecipe setMinimum(int minimum) {
			this.minimum = minimum;
			return this;
		}

		public ItemRecipe setNeedsWater() {
			this.needsWater = true;
			return this;
		}

		public ItemRecipe setNeedCauldron() {
			this.inCauldron = true;
			return this;
		}

		public int getMinimum() {
			return minimum;
		}

		public boolean doesNeedsWater() {
			return needsWater;
		}

		public boolean doesNeedCauldron() {
			return inCauldron;
		}
	}
}
