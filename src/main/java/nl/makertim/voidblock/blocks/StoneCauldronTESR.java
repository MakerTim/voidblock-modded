package nl.makertim.voidblock.blocks;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.fluids.Fluid;
import nl.makertim.voidblock.util.ClientUtil;
import nl.makertim.voidblock.util.RenderUtil;

public class StoneCauldronTESR extends TileEntitySpecialRenderer<StoneCauldronTileEntity> {

	private Minecraft mc = Minecraft.getMinecraft();
	private static ItemStack cauldron;

	@Override
	public void render(StoneCauldronTileEntity te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
		GlStateManager.translate(x, y, z);
		if (te.i == null) {
			te.i = new float[]{0};
		}
//		drawBase(te);
		drawLevel(te);
		GlStateManager.translate(-x, -y, -z);
	}

	private void drawBase(StoneCauldronTileEntity te) {
		GlStateManager.translate(0.5, 0.5, 0.5);
		GlStateManager.rotate(90, 1, 0, 0);
		if (cauldron == null) {
			cauldron = new ItemStack(BlockRegistration.stoneCauldron);
		}
		ClientUtil.renderItem(te.getPos(), cauldron, 0, 0, 0, 1f);
		GlStateManager.rotate(-90, 1, 0, 0);
		GlStateManager.translate(-0.5, -0.5, -0.5);
	}

	private void drawLevel(StoneCauldronTileEntity te) {
		if (te.getWorld().getBlockState(te.getPos().offset(EnumFacing.UP)).isFullBlock()) {
			return;
		}
		float progress = te.getInternalFluidLevel() / (float) Fluid.BUCKET_VOLUME;
		float animation = animateAndGetModifier(te) / 100 - 0.005f;
		if (progress > 0f) {
			int i = 0;
			float[] xD = getDirection();
			for (float xC = xD[0]; xC < xD[1]; xC += xD[2]) {
				i++;
				for (float yC = xD[0]; yC < xD[1]; yC += xD[2]) {
					i++;
					float rng = i % 3 == 0 ? animation : -animation;
					RenderUtil.renderFluidCuboid(te.getInternalFluid(), te.getPos().offset(EnumFacing.UP),
							0, 0, 0,
							xC, 2F / 16F + 14F / 16F * progress, yC,
							xC + xD[2], 2F / 16F + 14F / 16F * progress + rng, yC + xD[2]);
				}
			}

		}
	}

	private float[] getDirection() {
		if (mc.gameSettings.fancyGraphics) {
			return new float[]{
					2F / 16F, 14F / 16F, 1F / 16F
			};
		} else {
			return new float[]{
					2F / 16F, 14F / 16F, 4F / 16F
			};
		}
	}

	private float animateAndGetModifier(StoneCauldronTileEntity te) {
		te.i[0]++;
		if (te.i[0] < 100) {
			return te.i[0] / 100F;
		} else if (te.i[0] > 200) {
			te.i[0] = 0;
			return 0;
		}
		return 1F + ((100 - te.i[0]) / 100F);
	}
}
