package nl.makertim.voidblock.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import nl.makertim.voidblock.VoidblockMod;

import javax.annotation.Nullable;
import java.util.stream.Stream;

@SuppressWarnings("deprecation")
public class StoneAnvileBlock extends Block implements ITileEntityProvider {

	private static final AxisAlignedBB BOX = new AxisAlignedBB(4F / 16F, 0F, 3F / 16F, 12F / 16F, 7F / 16F, 13F / 16F);

	public StoneAnvileBlock() {
		super(Material.ROCK, MapColor.STONE);
		setHardness(3F);
		setSoundType(SoundType.STONE);
		setLightLevel(1 / 16F);
		setCreativeTab(VoidblockMod.creativeTab);
	}

	@SuppressWarnings({"deprecation"})
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
		return BOX;
	}

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
		StoneAnvileTileEntity tileEntity;
		TileEntity te = worldIn.getTileEntity(pos);
		if (te instanceof StoneAnvileTileEntity) {
			tileEntity = (StoneAnvileTileEntity) te;
		} else {
			return;
		}

		super.breakBlock(worldIn, pos, state);
		Stream.of(tileEntity.getItemStacks()).forEach(item -> //
				InventoryHelper.spawnItemStack(worldIn, pos.getX() + 0.5D, pos.getY() + 0.5D, pos.getZ() + 0.5D, item));
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		if (facing != EnumFacing.UP) {
			return false;
		}
		TileEntity te = worldIn.getTileEntity(pos);
		if (!(te instanceof StoneAnvileTileEntity)) {
			return false;
		}
		if (playerIn.isSneaking()) {
			return false;
		}

		if (VoidblockMod.proxyUtil.isLocal(playerIn)) {
			return true;
		}
		StoneAnvileTileEntity tileEntity = (StoneAnvileTileEntity) te;
		VoidblockMod.proxyUtil.swapItems(playerIn, hand, tileEntity, hitZ < 0.64 ? 0 : 1);
		return true;
	}

	@Override
	public boolean doesSideBlockRendering(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing face) {
		return false;
	}

	@Override
	public boolean isPassable(IBlockAccess worldIn, BlockPos pos) {
		return true;
	}

	@Override
	public boolean isFullCube(IBlockState state) {
		return false;
	}

	@Override
	public int getLightOpacity(IBlockState state, IBlockAccess world, BlockPos pos) {
		return 0;
	}

	@Nullable
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new StoneAnvileTileEntity(worldIn);
	}
}
