package nl.makertim.voidblock.blocks;

import net.minecraft.block.Block;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import nl.makertim.voidblock.VoidblockMod;

import java.util.Arrays;

public class BlockRegistration {

	public static StoneCrafterBlock stoneCrafter;
	public static StoneCauldronBlock stoneCauldron;
	public static StoneAnvileBlock stoneAnvile;

	public static void init(RegistryEvent.Register<Block> event) {
		stoneCrafter = (StoneCrafterBlock) new StoneCrafterBlock().setUnlocalizedName(VoidblockMod.MODID + ".stonecrafting").setRegistryName(VoidblockMod.MODID + ":stonecrafting");
		GameRegistry.registerTileEntity(StoneCrafterTileEntity.class, new ResourceLocation(VoidblockMod.MODID + ":stonecraftingTE"));

		stoneCauldron = (StoneCauldronBlock) new StoneCauldronBlock().setUnlocalizedName(VoidblockMod.MODID + ".stonecauldron").setRegistryName(VoidblockMod.MODID + ":stonecauldron");
		GameRegistry.registerTileEntity(StoneCauldronTileEntity.class, new ResourceLocation(VoidblockMod.MODID + ":stonecauldronTE"));

		stoneAnvile = (StoneAnvileBlock) new StoneAnvileBlock().setUnlocalizedName(VoidblockMod.MODID + ".stoneanvile").setRegistryName(VoidblockMod.MODID + ":stoneanvile");
		GameRegistry.registerTileEntity(StoneAnvileTileEntity.class, new ResourceLocation(VoidblockMod.MODID + ":stoneanvileTE"));

		Arrays.stream(getBlocks()).forEach(block -> block.setCreativeTab(VoidblockMod.creativeTab));
		Arrays.stream(getBlocks()).forEach(block -> event.getRegistry().register(block));
		if (VoidblockMod.proxyUtil.isClient()) {
			ClientBlockRegistration.init();
		}
	}

	public static Block[] getBlocks() {
		return new Block[]{
				stoneCrafter,
				stoneCauldron,
				stoneAnvile,
		};
	}
}
