package nl.makertim.voidblock.blocks;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;

import static nl.makertim.voidblock.util.ClientUtil.renderItem;

public class StoneAnvileTESR extends TileEntitySpecialRenderer<StoneAnvileTileEntity> {

	@Override
	public void render(StoneAnvileTileEntity te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
		GlStateManager.translate(x, y, z);
		if (te.i == null) {
			te.i = new float[]{0};
		}

		ItemStack[] items = te.getItemStacks();
		ItemStack crafting = items[0];
		ItemStack output = items[1];

		if (!crafting.isEmpty()) {
			te.i[0] = te.step * 65F;
			drawItem(te.getPos(), crafting, 0.5f, 0.45f, 0.5f, te.i[0], 0.18f);
			for (int i = 1; i < crafting.getCount(); i++) {
				drawItem(te.getPos(), crafting, 0.5f, 0.45f + ((i - 1) / 96F), 0.27f, 25F * ((i % 3 == 1) ? i : i - 3), 0.17f);
			}
		}
		if (!output.isEmpty()) {
			for (int i = 0; i < output.getCount(); i++) {
				drawItem(te.getPos(), output, 0.5f, 0.45f + ((i - 1) / 96F), 0.67f, 15F * ((i % 3 == 1) ? i : i - 3), 0.17f);
			}
		}

		GlStateManager.translate(-x, -y, -z);
	}

	private void drawItem(BlockPos pos, ItemStack is, float x, float y, float z, float rotate, float scale) {
		GlStateManager.translate(x, y, z);
		GlStateManager.rotate(rotate, 0, 1, 0);
		renderItem(pos, is, 0, 0, 0, scale);
		GlStateManager.rotate(-rotate, 0, 1, 0);
		GlStateManager.translate(-x, -y, -z);
	}
}
