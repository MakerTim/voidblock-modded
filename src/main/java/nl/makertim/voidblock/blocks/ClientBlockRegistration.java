package nl.makertim.voidblock.blocks;

import net.minecraftforge.fml.client.registry.ClientRegistry;

public class ClientBlockRegistration {

	public static void init() {
		ClientRegistry.bindTileEntitySpecialRenderer(StoneCrafterTileEntity.class, new StoneCrafterTESR());
		ClientRegistry.bindTileEntitySpecialRenderer(StoneCauldronTileEntity.class, new StoneCauldronTESR());
		ClientRegistry.bindTileEntitySpecialRenderer(StoneAnvileTileEntity.class, new StoneAnvileTESR());
	}
}
