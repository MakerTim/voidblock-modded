package nl.makertim.voidblock.blocks;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import nl.makertim.voidblock.VoidblockMod;
import nl.makertim.voidblock.util.RecipeHelper;

import javax.annotation.Nullable;
import java.util.Optional;

public class StoneCrafterTileEntity extends AnimatedTileEntity implements ITickable, IInventory {

	private NonNullList<ItemStack> itemStacks = NonNullList.withSize(9, ItemStack.EMPTY);
	private ItemStackHandler itemStacksHandler;
	public static final int TOTAL_CRAFT_TIME = 20 * 2;
	private static final String CRAFT_TIME_KEY = "craftTime";
	private int craftTime;
	private static final String CUSTOM_NAME_KEY = "customName";
	private String customName;
	private EntityPlayer lastPlayer;

	public static int rotation(EnumFacing facing) {
		switch (facing) {
		default:
		case NORTH:
			return 0;
		case WEST:
			return 1;
		case SOUTH:
			return 2;
		case EAST:
			return 3;
		}
	}

	public StoneCrafterTileEntity() {
		super();
	}

	public StoneCrafterTileEntity(World world) {
		super();
		setWorld(world);
	}

	public void setLastPlayer(EntityPlayer lastPlayer) {
		this.lastPlayer = lastPlayer;
	}

	@Override
	public void update() {
		if (world.isRemote || craftTime > TOTAL_CRAFT_TIME) {
			return;
		}

		if (craftTime == TOTAL_CRAFT_TIME) {
			Optional<IRecipe> recipeOptional = RecipeHelper.findRecipe(itemStacks, world);
			if (recipeOptional.isPresent()) {
				IRecipe recipe = recipeOptional.get();

				ItemStack result = recipe.getRecipeOutput().copy();
				itemStacks.clear();
				double x = pos.getX() + 0.5;
				double y = pos.getY() + 1.0;
				double z = pos.getZ() + 0.5;

				FMLCommonHandler.instance().firePlayerCraftingEvent(lastPlayer, result, this);
				InventoryHelper.spawnItemStack(world, x, y, z, result);
				world.playSound(null, getPos(), SoundEvents.UI_BUTTON_CLICK, SoundCategory.BLOCKS, 0.25F, 1.0F);
			}
			world.notifyBlockUpdate(pos, world.getBlockState(getPos()), world.getBlockState(getPos()), 2);
		}

		craftTime++;
	}

	public NonNullList<ItemStack> getItemStacks() {
		return itemStacks;
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);

		VoidblockMod.proxyUtil.saveAllItems(compound, itemStacks, true);
		compound.setInteger(CRAFT_TIME_KEY, craftTime);
		if (customName != null) {
			compound.setString(CUSTOM_NAME_KEY, customName);
		}
		return compound;
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);

		ItemStackHelper.loadAllItems(compound, itemStacks);
		craftTime = compound.getInteger(CRAFT_TIME_KEY);
		if (compound.hasKey(CUSTOM_NAME_KEY)) {
			customName = compound.getString(CUSTOM_NAME_KEY);
		}
	}

	@Override
	public int getSizeInventory() {
		return itemStacks.size();
	}

	@Override
	public boolean isEmpty() {
		for (ItemStack itemstack : this.itemStacks) {
			if (!itemstack.isEmpty()) {
				return false;
			}
		}

		return true;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return this.itemStacks.get(index);
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		return ItemStackHelper.getAndSplit(this.itemStacks, index, count);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		return ItemStackHelper.getAndRemove(this.itemStacks, index);
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		ItemStack itemstack = itemStacks.get(index);
		boolean flag = !stack.isEmpty() && stack.isItemEqual(itemstack) && ItemStack.areItemStackTagsEqual(stack, itemstack);
		itemStacks.set(index, stack);

		if (stack.getCount() > getInventoryStackLimit()) {
			stack.setCount(getInventoryStackLimit());
		}
		if (!flag) {
			world.playSound(null, getPos(), SoundEvents.ENTITY_ITEM_PICKUP, SoundCategory.BLOCKS, 0.25F, 1.0F);
			this.markDirty();
		}
	}

	@Override
	public int getInventoryStackLimit() {
		return 1;
	}

	@Override
	public boolean isUsableByPlayer(EntityPlayer player) {
		if (this.world.getTileEntity(this.pos) != this) {
			return false;
		} else {
			return player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) <= 64.0D;
		}
	}

	@Override
	public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
		return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ||
				super.hasCapability(capability, facing);
	}

	@Nullable
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
		if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			if (itemStacksHandler == null) {
				itemStacksHandler = new ItemStackHandler(itemStacks);
			}
			return (T) itemStacksHandler;
		}
		return super.getCapability(capability, facing);
	}

	@Override
	public void openInventory(EntityPlayer player) {
	}

	@Override
	public void closeInventory(EntityPlayer player) {
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		return true;
	}

	@Override
	public int getField(int id) {
		if (id == 0) {
			return craftTime;
		} else {
			return 0;
		}
	}

	@Override
	public void setField(int id, int value) {
		if (id == 0) {
			this.craftTime = value;
		}
	}

	@Override
	public int getFieldCount() {
		return 1;
	}

	@Override
	public void clear() {
		itemStacks.clear();
	}

	@Override
	public String getName() {
		return this.hasCustomName() ? this.customName : "container.crafting";
	}

	@Override
	public boolean hasCustomName() {
		return this.customName != null && !this.customName.isEmpty();
	}

	public void setCustomInventoryName(String customName) {
		this.customName = customName;
	}

	@Override
	public void markDirty() {
		super.markDirty();
		craftTime = 0;
	}
}
