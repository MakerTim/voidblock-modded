package nl.makertim.voidblock.blocks;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.CapabilityItemHandler;
import nl.makertim.voidblock.util.ExtendedItemStackHandler;
import nl.makertim.voidblock.util.OreDictUtil;

import javax.annotation.Nullable;
import java.util.Optional;

public class StoneAnvileTileEntity extends AnimatedTileEntity implements IInventory {

	private ExtendedItemStackHandler input = new ExtendedItemStackHandler(this, 1);
	private ExtendedItemStackHandler output = new ExtendedItemStackHandler(this, 1);

	public int step;

	public StoneAnvileTileEntity() {
		super();
		input.slotLimit = getInventoryStackLimit();
		input.acceptItemPredicate = (slot, itemStack) ->
				OreDictUtil.getPlateForItem(itemStack);
	}

	public StoneAnvileTileEntity(World world) {
		this();
		setWorld(world);
	}

	public void doStep() {
		if (!input.getStackInSlot(0).isEmpty() && step++ > 3) {
			step = 0;
			Optional<ItemStack> optionalPlate = input.acceptItemPredicate.apply(0, input.getStackInSlot(0));
			if (optionalPlate.isPresent()) {
				ItemStack plate = optionalPlate.get();
				output.insertItem(0, plate, false);
			}
			decrStackSize(0, 1);
			markDirty();
		}
	}

	public ItemStack[] getItemStacks() {
		return new ItemStack[]{input.getStackInSlot(0), output.getStackInSlot(0)};
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);

		compound.setTag("input", input.serializeNBT());
		compound.setTag("output", output.serializeNBT());
		compound.setInteger("step", step);

		return compound;
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);

		input.deserializeNBT(compound.getCompoundTag("input"));
		output.deserializeNBT(compound.getCompoundTag("output"));
		step = compound.getInteger("step");
	}

	@Override
	public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
		return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ||
				super.hasCapability(capability, facing);
	}

	@Nullable
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
		if (capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			return (T) (facing == EnumFacing.DOWN ? output : input);
		}
		return super.getCapability(capability, facing);
	}

	@Override
	public int getSizeInventory() {
		return input.getSlots();
	}

	@Override
	public boolean isEmpty() {
		boolean isEmpty = true;
		for (int i = 0; i < input.getSlots(); i++) {
			isEmpty = isEmpty && input.getStackInSlot(i).isEmpty();
		}
		for (int i = 0; i < output.getSlots(); i++) {
			isEmpty = isEmpty && output.getStackInSlot(i).isEmpty();
		}
		return isEmpty;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		if (index >= input.getSlots()) {
			return output.getStackInSlot(index - input.getSlots());
		}
		return input.getStackInSlot(index);
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		if (index >= input.getSlots()) {
			return output.extractItem(index - input.getSlots(), count, false);
		}
		return input.extractItem(index, count, false);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		if (index >= input.getSlots()) {
			return output.extractItem(index - input.getSlots(), output.getStackInSlot(index - input.getSlots()).getCount(), false);
		}
		return input.extractItem(index, input.getStackInSlot(index).getCount(), false);
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		if (index >= input.getSlots()) {
			output.setStackInSlot(index - input.getSlots(), stack);
		} else {
			input.setStackInSlot(index, stack);
		}
	}

	@Override
	public int getInventoryStackLimit() {
		return 10;
	}

	@Override
	public boolean isUsableByPlayer(EntityPlayer player) {
		return true;
	}

	@Override
	public void openInventory(EntityPlayer player) {
	}

	@Override
	public void closeInventory(EntityPlayer player) {
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		ExtendedItemStackHandler handler = input;
		if (index >= input.getSlots()) {
			handler = output;
			index -= input.getSlots();
		}
		return stack.isEmpty() || (handler.acceptItem(index, stack) &&
				(handler.getStackInSlot(index).isItemEqual(stack) || handler.getStackInSlot(index).isEmpty()) &&
				handler.getStackInSlot(index).getCount() < handler.getSlotLimit(index));
	}

	@Override
	public int getField(int id) {
		return 0;
	}

	@Override
	public void setField(int id, int value) {
	}

	@Override
	public int getFieldCount() {
		return 0;
	}

	@Override
	public void clear() {
		input = new ExtendedItemStackHandler(this, input.getSlots());
		output = new ExtendedItemStackHandler(this, output.getSlots());
	}

	@Override
	public String getName() {
		return "anvile";
	}

	@Override
	public boolean hasCustomName() {
		return false;
	}

	public void markUpdate() {
		world.notifyBlockUpdate(pos, world.getBlockState(getPos()), world.getBlockState(getPos()), 2);
	}

	@Override
	public void markDirty() {
		super.markDirty();
		markUpdate();
	}
}
