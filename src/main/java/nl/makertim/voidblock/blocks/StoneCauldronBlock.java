package nl.makertim.voidblock.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.*;
import nl.makertim.voidblock.VoidblockMod;

import javax.annotation.Nullable;
import java.util.List;

@SuppressWarnings("deprecation")
public class StoneCauldronBlock extends Block implements ITileEntityProvider {

	protected static final AxisAlignedBB AABB_LEGS = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.3125D, 1.0D);
	protected static final AxisAlignedBB AABB_WALL_NORTH = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.125D);
	protected static final AxisAlignedBB AABB_WALL_SOUTH = new AxisAlignedBB(0.0D, 0.0D, 0.875D, 1.0D, 1.0D, 1.0D);
	protected static final AxisAlignedBB AABB_WALL_EAST = new AxisAlignedBB(0.875D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
	protected static final AxisAlignedBB AABB_WALL_WEST = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.125D, 1.0D, 1.0D);

	public StoneCauldronBlock() {
		super(Material.ROCK, MapColor.STONE);
		setHardness(2F);
		setSoundType(SoundType.STONE);
		setLightLevel(1 / 16F);
		setCreativeTab(VoidblockMod.creativeTab);
	}

	@Override
	public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox, List<AxisAlignedBB> collidingBoxes, @Nullable Entity entityIn, boolean p_185477_7_) {
		addCollisionBoxToList(pos, entityBox, collidingBoxes, AABB_LEGS);
		addCollisionBoxToList(pos, entityBox, collidingBoxes, AABB_WALL_WEST);
		addCollisionBoxToList(pos, entityBox, collidingBoxes, AABB_WALL_NORTH);
		addCollisionBoxToList(pos, entityBox, collidingBoxes, AABB_WALL_EAST);
		addCollisionBoxToList(pos, entityBox, collidingBoxes, AABB_WALL_SOUTH);
	}

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
		StoneCauldronTileEntity tileEntity;
		TileEntity te = worldIn.getTileEntity(pos);
		if (te instanceof StoneCauldronTileEntity) {
			tileEntity = (StoneCauldronTileEntity) te;
		} else {
			return;
		}

		super.breakBlock(worldIn, pos, state);
		if (tileEntity.getInternalFluidLevel() == Fluid.BUCKET_VOLUME) {
			worldIn.setBlockState(pos, tileEntity.getInternalFluid().getFluid().getBlock().getDefaultState(), 11);
			tileEntity.getInternalFluid().getFluid().getBlock().getDefaultState().neighborChanged(worldIn, pos, state.getBlock(), pos);
			InventoryHelper.spawnItemStack(worldIn, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(this));
		}
	}

	@Override
	public boolean isOpaqueCube(IBlockState state) {
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state) {
		return false;
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		ItemStack itemstack = playerIn.getHeldItem(hand);

		if (itemstack.isEmpty()) {
			return false;
		} else {
			StoneCauldronTileEntity tileEntity;
			TileEntity te = worldIn.getTileEntity(pos);
			if (te instanceof StoneCauldronTileEntity) {
				tileEntity = (StoneCauldronTileEntity) te;
			} else {
				return false;
			}
			Item item = itemstack.getItem();

			if (!tileEntity.isFull()) {
				if ((item == Items.WATER_BUCKET && tileEntity.hasWater()) || (item == Items.LAVA_BUCKET && tileEntity.hasLava())) {
					if (!worldIn.isRemote) {
						if (!playerIn.capabilities.isCreativeMode) {
							playerIn.setHeldItem(hand, new ItemStack(Items.BUCKET));
						}

						playerIn.addStat(StatList.CAULDRON_FILLED);
						tileEntity.fillFluid(item == Items.WATER_BUCKET ? FluidRegistry.WATER : FluidRegistry.LAVA);
						worldIn.playSound(null, pos, SoundEvents.ITEM_BUCKET_EMPTY, SoundCategory.BLOCKS, 1.0F, 1.0F);
					}

					return true;
				} else if (item instanceof UniversalBucket) {
					UniversalBucket bucket = (UniversalBucket) item;
					FluidStack insideBucket = bucket.getFluid(itemstack);

					if (!tileEntity.hasSameFluid(insideBucket.getFluid())) {
						return false;
					}
					if (!worldIn.isRemote) {
						if (!playerIn.capabilities.isCreativeMode) {
							itemstack.shrink(1);
							VoidblockMod.proxyUtil.giveOrDrop(playerIn, new ItemStack(Items.BUCKET));
						}

						playerIn.addStat(StatList.CAULDRON_USED);
						tileEntity.fillFluid(insideBucket);
						worldIn.playSound(null, pos, SoundEvents.ITEM_BUCKET_EMPTY, SoundCategory.BLOCKS, 1.0F, 1.0F);
					}

					return true;
				}
			} else {
				if (item == Items.BUCKET) {
					if (!worldIn.isRemote) {
						if (!playerIn.capabilities.isCreativeMode) {
							itemstack.shrink(1);

							ItemStack filledBucket = FluidUtil.getFilledBucket(tileEntity.getInternalFluid());
							VoidblockMod.proxyUtil.giveOrDrop(playerIn, filledBucket);
						}

						playerIn.addStat(StatList.CAULDRON_USED);
						tileEntity.emptyFluid();
						worldIn.playSound(null, pos, SoundEvents.ITEM_BUCKET_FILL, SoundCategory.BLOCKS, 1.0F, 1.0F);
					}

					return true;
				}
			}
			return false;
		}
	}

	@Override
	public boolean isPassable(IBlockAccess worldIn, BlockPos pos) {
		return true;
	}

	@Override
	public BlockFaceShape getBlockFaceShape(IBlockAccess p_193383_1_, IBlockState p_193383_2_, BlockPos p_193383_3_, EnumFacing p_193383_4_) {
		if (p_193383_4_ == EnumFacing.UP) {
			return BlockFaceShape.BOWL;
		} else {
			return p_193383_4_ == EnumFacing.DOWN ? BlockFaceShape.UNDEFINED : BlockFaceShape.SOLID;
		}
	}

	@Override
	public void onEntityCollidedWithBlock(World worldIn, BlockPos pos, IBlockState state, Entity entityIn) {
		TileEntity te = worldIn.getTileEntity(pos);
		if (te instanceof StoneCauldronTileEntity) {
			StoneCauldronTileEntity tileEntity = (StoneCauldronTileEntity) te;
			int water = tileEntity.getInternalFluidLevel();
			int i = water / 1000;
			float f = (float) pos.getY() + (6.0F + (float) (3 * i)) / 16.0F;
			if (!worldIn.isRemote && entityIn.isBurning() && i > 0 && entityIn.getEntityBoundingBox().minY <= (double) f) {
				entityIn.extinguish();
				tileEntity.emptyFluid();
			}
		}
	}

	@Override
	public boolean doesSideBlockRendering(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing face) {
		return false;
	}

	@Nullable
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new StoneCauldronTileEntity(worldIn);
	}
}
