package nl.makertim.voidblock.blocks;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;

import static nl.makertim.voidblock.util.ClientUtil.renderItem;

public class StoneCrafterTESR extends TileEntitySpecialRenderer<StoneCrafterTileEntity> {

	private Minecraft mc = Minecraft.getMinecraft();
	private static ItemStack table;

	@Override
	public void render(StoneCrafterTileEntity te, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
		GlStateManager.translate(x, y, z);
		if (te.i == null) {
			te.i = new float[]{-1, 100};
		}
		drawBase(te);
		drawItems(te);
		GlStateManager.translate(-x, -y, -z);
	}

	private void drawBase(StoneCrafterTileEntity te) {
		GlStateManager.translate(0.5, 0.6, 0.5);
		GlStateManager.rotate(90, 1, 0, 0);
		if (table == null) {
			table = new ItemStack(BlockRegistration.stoneCrafter);
		}
		renderItem(te.getPos(), table, 0, 0, 0, 1.2f);
		GlStateManager.rotate(-90, 1, 0, 0);
		GlStateManager.translate(-0.5, -0.6, -0.5);
	}

	private void drawItems(StoneCrafterTileEntity te) {
		int yawI = StoneCrafterTileEntity.rotation(mc.player.getHorizontalFacing());

		float modifier = animateAndGetModifier(te, yawI);

		GlStateManager.translate(0.5, 0, 0.5);
		rotateOnYaw(te.i[0]);
		for (int i = 0; i < 9; i++) {
			ItemStack itemStack = te.getStackInSlot(i);
			if (itemStack.isEmpty()) {
				continue;
			}
			float xOff = (i % 3) - 1.5f;
			float zOff = ((int) (i / 3F)) - 1.5f;
			xOff += 0.5;
			zOff += 0.5;
			xOff /= 3;
			zOff /= 3;

			renderItem(te.getPos(), itemStack, xOff, 15F / 16F - ((1 - modifier) / 10), zOff, 0.2f * modifier);
		}
		rotateBackOnYaw(te.i[0]);
		GlStateManager.translate(-0.5, 0, -0.5);
	}

	private float animateAndGetModifier(StoneCrafterTileEntity te, int yawI) {
		if (te.i[0] != yawI && te.i[1] >= 100) {
			te.i[1] = 0;
		}
		if (te.i[1] < 100) {
			te.i[1] += Math.max(1, (30D / Minecraft.getDebugFPS()) * 2D) * 3D;
		}
		float modifier = 1;
		if (te.i[1] < 50) {
			modifier = (100F - (te.i[1] * 2F)) / 100F;
		} else if (te.i[1] < 100) {
			modifier = ((te.i[1] * 2F) - 100F) / 100F;
		}
		if (te.i[1] >= 50 && te.i[0] != yawI) {
			te.i[0] = yawI;
		}
		return modifier;
	}

	protected void rotateOnYaw(float i) {
		GlStateManager.rotate(i * 90F, 0, 1, 0);
	}

	protected void rotateBackOnYaw(float i) {
		rotateOnYaw(-i);
	}
}
