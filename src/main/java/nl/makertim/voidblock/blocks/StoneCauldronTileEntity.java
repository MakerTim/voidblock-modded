package nl.makertim.voidblock.blocks;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;

import javax.annotation.Nullable;
import java.util.List;

public class StoneCauldronTileEntity extends AnimatedTileEntity implements ITickable {

	private FluidStack internal = new FluidStack(FluidRegistry.WATER, 0);

	public StoneCauldronTileEntity() {
		super();
	}

	public StoneCauldronTileEntity(World world) {
		super();
		setWorld(world);
	}

	@Override
	public void update() {
		if (isFull() || !hasWater()) {
			return;
		}
		if (internal.getFluid() != FluidRegistry.WATER) {
			internal = new FluidStack(FluidRegistry.WATER, internal.amount);
		}
		List<EntityPlayer> playersInside = world.getEntitiesWithinAABB(EntityPlayer.class, getWorld().getBlockState(getPos()).getBoundingBox(world, pos).offset(pos));
		if (world.rand.nextInt(Fluid.BUCKET_VOLUME) < 123 * (playersInside.size() + 1)) {
			internal.amount++;
		}
		if (getInternalFluidLevel() % 100 == 0 || isFull()) {
			world.notifyBlockUpdate(pos, world.getBlockState(getPos()), world.getBlockState(getPos()), 2);
		}
	}

	public int getInternalFluidLevel() {
		return internal.amount;
	}

	public FluidStack getInternalFluid() {
		return internal;
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);

		NBTTagCompound nbtTank = new NBTTagCompound();
		internal.writeToNBT(nbtTank);
		compound.setTag("tank", nbtTank);

		return compound;
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);

		NBTTagCompound nbtTank = compound.getCompoundTag("tank");
		internal = FluidStack.loadFluidStackFromNBT(nbtTank);
	}

	@Override
	public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
		return capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY ||
				super.hasCapability(capability, facing);
	}

	@Nullable
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
		if (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY) {
			return (T) new FluidTank(internal, Fluid.BUCKET_VOLUME);
		}
		return super.getCapability(capability, facing);
	}

	public void emptyFluid() {
		internal.amount = 0;
		world.notifyBlockUpdate(pos, world.getBlockState(getPos()), world.getBlockState(getPos()), 2);
	}

	public boolean isFull() {
		return internal.amount >= Fluid.BUCKET_VOLUME;
	}

	public boolean isEmpty() {
		return internal.amount <= 10;
	}

	public boolean hasWater() {
		return hasSameFluid(FluidRegistry.WATER);
	}

	public boolean hasLava() {
		return hasSameFluid(FluidRegistry.LAVA);
	}

	public boolean hasSameFluid(Fluid fluid) {
		return internal.getFluid() == fluid || isEmpty();
	}

	public void fillFluid(Fluid fluid) {
		internal = new FluidStack(fluid, Fluid.BUCKET_VOLUME);
		world.notifyBlockUpdate(pos, world.getBlockState(getPos()), world.getBlockState(getPos()), 2);
	}

	public void fillFluid(FluidStack fluid) {
		internal = fluid;
		world.notifyBlockUpdate(pos, world.getBlockState(pos), world.getBlockState(pos), 2);
	}
}
