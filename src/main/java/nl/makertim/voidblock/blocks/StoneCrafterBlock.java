package nl.makertim.voidblock.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import nl.makertim.voidblock.VoidblockMod;

import javax.annotation.Nullable;

public class StoneCrafterBlock extends BlockContainer implements ITileEntityProvider {

	private static final AxisAlignedBB BOX = new AxisAlignedBB(0.05D, 0.0D, 0.05D, 0.95D, 0.9D, 0.95D);

	public StoneCrafterBlock() {
		super(Material.ROCK);
		setHardness(1.75F);
		setSoundType(SoundType.STONE);
		setLightLevel(1 / 16F);
		setCreativeTab(VoidblockMod.creativeTab);
	}

	@SuppressWarnings({"deprecation"})
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
		return BOX;
	}

	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
		if (hasTileEntity(state)) {
			TileEntity te = worldIn.getTileEntity(pos);
			if (te instanceof StoneCrafterTileEntity) {
				StoneCrafterTileEntity tileEntity = (StoneCrafterTileEntity) te;
				InventoryHelper.dropInventoryItems(worldIn, pos, tileEntity);
			}
		}
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		if (facing != EnumFacing.UP) {
			return false;
		}
		TileEntity te = worldIn.getTileEntity(pos);
		if (!(te instanceof StoneCrafterTileEntity)) {
			return false;
		}
		if (playerIn.isSneaking()) {
			return false;
		}
		((StoneCrafterTileEntity) te).setLastPlayer(playerIn);
		if (VoidblockMod.proxyUtil.isLocal(playerIn)) {
			return true;
		}

		StoneCrafterTileEntity tileEntity = (StoneCrafterTileEntity) te;

		int xIndex = (int) (hitX * 3);
		int zIndex = (int) (hitZ * 3);
		int index = getIndexFromHitAndRotation(StoneCrafterTileEntity.rotation(playerIn.getHorizontalFacing()), xIndex, zIndex);

		VoidblockMod.proxyUtil.swapItems(playerIn, hand, tileEntity, index);
		worldIn.notifyBlockUpdate(pos, state, state, 2);
		return true;
	}

	private int getIndexFromHitAndRotation(int facing, int xIndex, int zIndex) {
		switch (facing) {
		default:
		case 0:
			return xIndex + (zIndex * 3);
		case 1:
			return (2 - zIndex) + (xIndex * 3);
		case 2:
			return (2 - xIndex) + ((2 - zIndex) * 3);
		case 3:
			return zIndex + ((2 - xIndex) * 3);
		}
	}

	@Override
	public boolean doesSideBlockRendering(IBlockState state, IBlockAccess world, BlockPos pos, EnumFacing face) {
		return false;
	}

	@Nullable
	@Override
	public StoneCrafterTileEntity createNewTileEntity(World worldIn, int meta) {
		return new StoneCrafterTileEntity(worldIn);
	}
}
