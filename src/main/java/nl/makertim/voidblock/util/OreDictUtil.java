package nl.makertim.voidblock.util;

import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

public class OreDictUtil {

	public static Collection<String> getDictionariesFromStack(ItemStack itemStack) {
		if (itemStack.isEmpty()) {
			return new HashSet<>();
		}
		int[] oreDictIds = OreDictionary.getOreIDs(itemStack);
		return Arrays.stream(oreDictIds).mapToObj(OreDictionary::getOreName).collect(Collectors.toSet());
	}

	public static Optional<ItemStack> searchItemByOreDict(String name) {
		return OreDictionary.getOres(name).stream().map(ItemStack::copy).findAny();
	}

	public static Optional<ItemStack> getPlateForItem(ItemStack input) {
		Collection<String> oreDictEntries = getDictionariesFromStack(input);
		Optional<String> optionalPlateEntry = oreDictEntries.stream()
				.filter(oreDict -> oreDict.contains("ingot"))
				.map(oreDict -> oreDict.replace("ingot", "plate"))
				.findFirst();
		if (optionalPlateEntry.isPresent()) {
			String entry = optionalPlateEntry.get();
			return searchItemByOreDict(entry);
		}
		return Optional.empty();
	}
}
