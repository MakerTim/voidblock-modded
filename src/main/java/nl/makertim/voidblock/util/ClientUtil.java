package nl.makertim.voidblock.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ClientUtil extends Util {

	@Override
	public boolean isServer() {
		return false;
	}

	@Override
	public boolean isClient() {
		return true;
	}

	@Override
	public NBTTagCompound fromPlayer(World world, UUID uuid) {
		return new NBTTagCompound();
	}

	@Override
	public Map<String, Number> fromPlayerStats(World world, UUID uuid) {
		return new HashMap<>();
	}

	public static void renderItem(BlockPos pos, ItemStack is, float xOff, float yOff, float zOff, float scale) {
		GlStateManager.pushMatrix();
		RenderHelper.enableStandardItemLighting();
		int lu = 0;
		int lv = 0;
		for (EnumFacing facing : EnumFacing.values()) {
			int ambLight = Minecraft.getMinecraft().world.getCombinedLight(pos.offset(facing), 0);
			lu = Math.max(lu, ambLight % 65536);
			lv = Math.max(lv, ambLight / 65536);
		}
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) lu / 1.0F, (float) lv / 1.0F);
		GlStateManager.translate(xOff, yOff, zOff);
		GlStateManager.scale(scale, scale, scale);
		GlStateManager.rotate(-90, 1, 0, 0);

		Minecraft.getMinecraft().getRenderItem().renderItem(is, ItemCameraTransforms.TransformType.NONE);

		GlStateManager.rotate(90, 1, 0, 0);
		GlStateManager.disableBlend();
		GlStateManager.popMatrix();
	}
}
