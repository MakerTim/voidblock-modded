package nl.makertim.voidblock.util;

import net.minecraft.client.util.RecipeItemHelper;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

import java.util.Optional;
import java.util.stream.StreamSupport;

public class RecipeHelper {

	public static Optional<IRecipe> findRecipe(NonNullList<ItemStack> itemStacks, World world) {
		return StreamSupport.stream(CraftingManager.REGISTRY.spliterator(), true) //
				.parallel() //
				.filter(iRecipe -> iRecipe.matches(new FakeInventory(itemStacks), world)) //
				.findFirst();
	}

	private static class FakeInventory extends InventoryCrafting {

		private NonNullList<ItemStack> stackList;
		private int size;

		public FakeInventory(NonNullList<ItemStack> itemStacks) {
			super(null, -1, -1);
			stackList = itemStacks;
			size = MathHelper.floor(Math.sqrt(itemStacks.size()));
		}

		@Override
		public int getSizeInventory() {
			return this.stackList.size();
		}

		@Override
		public boolean isEmpty() {
			for (ItemStack itemstack : this.stackList) {
				if (!itemstack.isEmpty()) {
					return false;
				}
			}
			return true;
		}

		@Override
		public ItemStack getStackInSlot(int index) {
			return index >= this.getSizeInventory() ? ItemStack.EMPTY : this.stackList.get(index);
		}

		@Override
		public ItemStack getStackInRowAndColumn(int row, int column) {
			return row >= 0 && row < this.size && column >= 0 && column <= this.size ?
					this.getStackInSlot(row + column * this.size) : ItemStack.EMPTY;
		}

		@Override
		public ItemStack removeStackFromSlot(int index) {
			return ItemStackHelper.getAndRemove(this.stackList, index);
		}

		@Override
		public ItemStack decrStackSize(int index, int count) {
			return ItemStackHelper.getAndSplit(this.stackList, index, count);
		}

		@Override
		public void setInventorySlotContents(int index, ItemStack stack) {
			this.stackList.set(index, stack);
		}

		@Override
		public void clear() {
			this.stackList.clear();
		}

		@Override
		public int getHeight() {
			return this.size;
		}

		@Override
		public int getWidth() {
			return this.size;
		}

		@Override
		public void func_194018_a(RecipeItemHelper recipeItemHelper) {
			for (ItemStack itemstack : this.stackList) {
				recipeItemHelper.func_194112_a(itemstack);
			}
		}
	}
}
