package nl.makertim.voidblock.util;

import com.google.gson.Gson;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraft.world.storage.SaveHandler;
import nl.makertim.voidblock.VoidblockMod;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ServerUtil extends Util {

	@Override
	public boolean isServer() {
		return true;
	}

	@Override
	public boolean isClient() {
		return false;
	}

	@Override
	public NBTTagCompound fromPlayer(World world, UUID uuid) {
		NBTTagCompound nbttagcompound = new NBTTagCompound();

		try {
			File playerFolder = new File(getWorldFile(world), "playerdata");
			File playerFile = new File(playerFolder, uuid + ".dat");

			if (playerFile.exists() && playerFile.isFile()) {
				nbttagcompound = CompressedStreamTools.readCompressed(new FileInputStream(playerFile));
			}
		} catch (Exception ex) {
			VoidblockMod.logger.error("Failed to load player data for {}", uuid);
		}

		return nbttagcompound;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Number> fromPlayerStats(World world, UUID uuid) {
		Map<String, Number> map = new HashMap<>();

		try {
			File playerFolder = new File(getWorldFile(world), "stats");
			File playerFile = new File(playerFolder, uuid + ".json");

			if (playerFile.exists() && playerFile.isFile()) {
				map = (Map<String, Number>) new Gson().fromJson(new FileReader(playerFile), Map.class);
			}
		} catch (Exception ex) {
			VoidblockMod.logger.error("Failed to load player data for {}", uuid);
		}

		return map;
	}

	private File getWorldFile(World world) {
		SaveHandler saveHandler = (SaveHandler) world.getSaveHandler();
		return saveHandler.getWorldDirectory();
	}
}
