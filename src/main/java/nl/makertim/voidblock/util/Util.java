package nl.makertim.voidblock.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;
import net.minecraftforge.common.UsernameCache;
import nl.makertim.voidblock.VoidblockMod;

import java.util.Map;
import java.util.UUID;

public abstract class Util {

	Util() {
	}

	public abstract boolean isServer();

	public abstract boolean isClient();

	public abstract NBTTagCompound fromPlayer(World world, UUID uuid);

	public abstract Map<String, Number> fromPlayerStats(World world, UUID uuid);

	public boolean isRemote(EntityPlayer player) {
		return !isLocal(player);
	}

	public boolean isLocal(EntityPlayer player) {
		return player.getClass().getName().contains("SP");
	}

	public UUID fromCache(String playerName) {
		return UsernameCache.getMap().entrySet().parallelStream() //
				.filter(set -> playerName.equals(set.getValue())) //
				.map(Map.Entry::getKey) //
				.findFirst() //
				.orElse(null);
	}

	public boolean isInArea(double x, double z, double centerX, double centerZ, double radius) {
		double dx = Math.abs(x - centerX);
		double dz = Math.abs(z - centerZ);

		if (dx > radius || dz > radius) {
			return false;
		}
		if (dx + dz <= radius) {
			return true;
		}

		return (dx * dx) + (dz * dz) <= (radius * radius);
	}

	public long unify(long i, long n) {
		long m = (long) Math.pow(10, n);
		return (i / m) * m;
	}

	public NBTTagCompound saveAllItems(NBTTagCompound tag, NonNullList<ItemStack> list, boolean saveEmpty) {
		NBTTagList nbttaglist = new NBTTagList();

		for (int i = 0; i < list.size(); ++i) {
			ItemStack itemstack = list.get(i);

			if (!itemstack.isEmpty() || saveEmpty) {
				NBTTagCompound nbttagcompound = new NBTTagCompound();
				nbttagcompound.setByte("Slot", (byte) i);
				itemstack.writeToNBT(nbttagcompound);
				nbttaglist.appendTag(nbttagcompound);
			}
		}

		if (!nbttaglist.hasNoTags() || saveEmpty) {
			tag.setTag("Items", nbttaglist);
		}

		return tag;
	}

	public void giveOrDrop(EntityPlayer player, ItemStack itemStack) {
		if (!player.inventory.addItemStackToInventory(itemStack)) {
			InventoryHelper.spawnItemStack(player.world, player.posX, player.posY, player.posZ, itemStack);
		}
	}

	public void swapItems(EntityPlayer playerIn, EnumHand hand, IInventory tileEntity, int index) {
		ItemStack itemStackTE = tileEntity.getStackInSlot(index);
		ItemStack inHand = playerIn.getHeldItem(hand);
		if (itemStackTE.isEmpty()) { // no item yet in TE
			if (!tileEntity.isItemValidForSlot(index, inHand)) {
				return;
			}
			itemStackTE = inHand.copy();
			int amount = Math.min(tileEntity.getInventoryStackLimit(), itemStackTE.getCount());
			itemStackTE.setCount(amount);
			inHand.shrink(amount);
			tileEntity.setInventorySlotContents(index, itemStackTE);
		} else if (!inHand.isItemEqual(itemStackTE) && tileEntity.isItemValidForSlot(index, inHand)) { // Item is diffrent as in TE -> swap
			ItemStack newContent = inHand.copy();
			int newAmount = Math.min(tileEntity.getInventoryStackLimit(), inHand.getCount());
			inHand.shrink(newAmount);
			newContent.setCount(newAmount);
			VoidblockMod.proxyUtil.giveOrDrop(playerIn, itemStackTE);
			tileEntity.setInventorySlotContents(index, newContent);
		} else { // Item is same or TE didn't accept new item -> remove item TE
			VoidblockMod.proxyUtil.giveOrDrop(playerIn, tileEntity.getStackInSlot(index));
			tileEntity.setInventorySlotContents(index, ItemStack.EMPTY);
		}
	}
}
