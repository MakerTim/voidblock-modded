package nl.makertim.voidblock.util;

import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.function.BiFunction;

public class ExtendedItemStackHandler extends ItemStackHandler {

	private TileEntity tileEntity;
	public int slotLimit = 64;
	public BiFunction<Integer, ItemStack, Optional<ItemStack>> acceptItemPredicate = (integer, itemStack) -> Optional.of(itemStack);

	public ExtendedItemStackHandler(TileEntity tileEntity) {
		this(tileEntity, 1);
	}

	public ExtendedItemStackHandler(TileEntity tileEntity, int size) {
		super(size);
		this.tileEntity = tileEntity;
	}

	@Nonnull
	@Override
	public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
		if (!acceptItem(slot, stack)) {
			return stack;
		}
		return super.insertItem(slot, stack, simulate);
	}

	public boolean acceptItem(int slot, ItemStack stack) {
		return acceptItemPredicate.apply(slot, stack).isPresent();
	}

	@Override
	protected void onContentsChanged(int slot) {
		super.onContentsChanged(slot);
		tileEntity.markDirty();
	}

	@Override
	public int getSlotLimit(int slot) {
		return slotLimit;
	}
}
