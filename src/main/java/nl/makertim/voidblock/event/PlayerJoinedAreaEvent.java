package nl.makertim.voidblock.event;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import nl.makertim.voidblock.area.Area;

@Cancelable
public class PlayerJoinedAreaEvent extends PlayerAreaEvent {

	public PlayerJoinedAreaEvent(EntityPlayer player, Area area) {
		super(player, area);
	}
}
