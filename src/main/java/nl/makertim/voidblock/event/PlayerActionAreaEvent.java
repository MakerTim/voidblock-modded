package nl.makertim.voidblock.event;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import nl.makertim.voidblock.area.Area;

@Cancelable
public class PlayerActionAreaEvent extends PlayerAreaEvent {

	public PlayerActionAreaEvent(EntityPlayer player, Area area) {
		super(player, area);
	}
}
