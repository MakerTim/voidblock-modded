package nl.makertim.voidblock.event;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import nl.makertim.voidblock.area.Area;

@Cancelable
public class PlayerAreaEvent extends PlayerEvent  {

	private Area area;

	public PlayerAreaEvent(EntityPlayer player, Area area) {
		super(player);
		this.area = area;
		setCanceled(false);
	}

	public Area getArea() {
		return area;
	}
}
