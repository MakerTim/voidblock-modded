package nl.makertim.voidblock.stages;

import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.AdvancementEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import nl.makertim.voidblock.stages.stone.StoneListener;

public class StagesRegistration {

	public static void init() {
		MinecraftForge.EVENT_BUS.register(StagesRegistration.class);
		// STONE
		MinecraftForge.EVENT_BUS.register(new StoneListener());
	}

	@SubscribeEvent
	public static void onAdvancement(AdvancementEvent event) {
		try {
//			event.getEntityPlayer().sendMessage(event.getAdvancement().getDisplayText());
//					new TextComponentTranslation("chat.type.advancement." +
//							event.getAdvancement().getDisplay().getFrame().getName(),
//							event.getEntityPlayer().getDisplayName(), event.getAdvancement().getDisplayText()));
		}catch (Exception ex){
			System.out.println();
		}

	}
}
