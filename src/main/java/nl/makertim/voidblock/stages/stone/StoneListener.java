package nl.makertim.voidblock.stages.stone;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import nl.makertim.voidblock.VoidblockMod;
import nl.makertim.voidblock.blocks.BlockRegistration;
import nl.makertim.voidblock.items.ItemRegistration;
import nl.makertim.voidblock.recipes.Recipes;
import nl.makertim.voidblock.recipes.Recipes.ItemRecipe;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.IntFunction;

public class StoneListener {

	protected static final float STONE_ROCK_CHANGE = 0.1F;
	protected static final float STONE_ROCK_AWAY_CHANGE = 0.1F;
	protected static final float STONE_SHARD_CHANGE = 0.2F;
	protected static final float STONE_SHARD_AWAY_CHANGE = 0.25F;
	protected static final float STONE_SHARD_LOST_CHANGE = 0.5F;

	@SubscribeEvent
	public void itemCrafting(TickEvent.WorldTickEvent event) {
		if (event.phase == TickEvent.Phase.START || event.world.isRemote) {
			return;
		}

		World world = event.world;
		Collection<Item> craftAbleItems = Recipes.itemRecipes.keySet();

		List<EntityItem> itemsCraftAble = world.getEntities(EntityItem.class, entity -> {
			return craftAbleItems.contains(entity.getItem().getItem());
		});
		itemsCraftAble.forEach(entityItem -> {
			List<ItemRecipe> recipes = Recipes.itemRecipes.get(entityItem.getItem().getItem());
			IBlockState block = world.getBlockState(entityItem.getPosition());
			boolean isInWater = block.getMaterial().isLiquid();
			boolean isInCauldron = block.getBlock() == BlockRegistration.stoneCauldron;

			Optional<ItemRecipe> recipeOptional = recipes.stream().filter(recipe ->
					recipe.doesNeedsWater() == isInWater &&
							recipe.doesNeedCauldron() == isInCauldron
			).findFirst();
			recipeOptional.ifPresent(recipe -> {
				tickItem(entityItem, count -> {
							ItemStack output = recipe.output.copy();
							output.setCount(Math.max(recipe.getMinimum(), (int) (count * recipe.chance)));
							return output;
						},
						recipe.ticks);
			});
		});
	}

	private void tickItem(EntityItem entity, IntFunction<ItemStack> itemStackFunction, long ticks) {
		entity.timeUntilPortal--;
		entity.setNoDespawn();
		if (entity.timeUntilPortal < -ticks) {
			entity.timeUntilPortal = 0;
			entity.setItem(itemStackFunction.apply(entity.getItem().getCount()));
		}
	}

	@SubscribeEvent
	public void onRightClickStoneRock(PlayerInteractEvent.LeftClickBlock event) {
		if (event.isCanceled() ||
				VoidblockMod.proxyUtil.isLocal(event.getEntityPlayer()) ||
				event.getHand() == EnumHand.OFF_HAND) {
			return;
		}

		EntityPlayer player = event.getEntityPlayer();
		World world = event.getWorld();
		IBlockState blockState = world.getBlockState(event.getPos());
		if (blockState != Blocks.STONE.getDefaultState()) {
			return;
		}
		if (!player.getHeldItem(event.getHand()).isEmpty()) {
			return;
		}
		if (world.rand.nextFloat() > STONE_ROCK_CHANGE) {
			return;
		}
		if (world.rand.nextFloat() < STONE_ROCK_AWAY_CHANGE) {
			world.setBlockToAir(event.getPos());
		}

		BlockPos pos = event.getEntityPlayer().getPosition();
		InventoryHelper.spawnItemStack(world, pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D, new ItemStack(ItemRegistration.rock, 1));
	}

	@SubscribeEvent
	public void onRightClickStoneShard(PlayerInteractEvent.LeftClickBlock event) {
		if (event.isCanceled() ||
				VoidblockMod.proxyUtil.isLocal(event.getEntityPlayer()) ||
				event.getHand() == EnumHand.OFF_HAND) {
			return;
		}

		World world = event.getWorld();
		EntityPlayer player = event.getEntityPlayer();
		ItemStack is = player.getHeldItem(event.getHand());
		IBlockState blockState = world.getBlockState(event.getPos());
		if (blockState != Blocks.STONE.getDefaultState()) {
			return;
		}
		if (player.getHeldItem(event.getHand()).getItem() != ItemRegistration.rock) {
			return;
		}
		if (world.rand.nextFloat() > STONE_SHARD_CHANGE) {
			return;
		}
		if (world.rand.nextFloat() < STONE_SHARD_AWAY_CHANGE) {
			world.setBlockToAir(event.getPos());
		}
		if (world.rand.nextFloat() < STONE_SHARD_LOST_CHANGE) {
			is.setCount(is.getCount() - 1);
			if (is.getCount() <= 0) {
				player.setHeldItem(event.getHand(), ItemStack.EMPTY);
			}
			player.getCooldownTracker().setCooldown(is.getItem(), 5);
		}

		BlockPos pos = event.getEntityPlayer().getPosition();
		InventoryHelper.spawnItemStack(world, pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D, new ItemStack(ItemRegistration.shard, 1));
	}

}
