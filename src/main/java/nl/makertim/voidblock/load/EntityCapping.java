package nl.makertim.voidblock.load;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ClassInheritanceMultiMap;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.Arrays;

public class EntityCapping {

	public static void init() {
		MinecraftForge.EVENT_BUS.register(new EntityCapping());
	}

	@SubscribeEvent
	public void onChunkLoad(ChunkEvent.Load event) {
		onChunkBalance(event.getChunk());
	}

	@SubscribeEvent
	public void onChunkUnload(ChunkEvent.Unload event) {
		onChunkBalance(event.getChunk());
	}

	private void onChunkBalance(Chunk chunk) {
		int sumEntities = Arrays.stream(chunk.getEntityLists()) //
				.mapToInt(ClassInheritanceMultiMap::size) //
				.sum();

		if (sumEntities >= 50) {
			for (ClassInheritanceMultiMap<Entity> subList : chunk.getEntityLists()) {
				subList.stream() //
					.filter(entity -> entity instanceof EntityLivingBase) //
					.map(entity -> (EntityLivingBase) entity) //
					.forEach(entity -> //
						entity.addPotionEffect(
							new PotionEffect(Potion.getPotionFromResourceLocation("wither"), 20, 1)));
			}
		}
	}
}
